---
title: "Achievements"
---

## Linux Kernel

<img src="/img/achievements/kernel_contributions.png"
     style="float:left; margin-right:30px">

The group accounted for 20% of all contributions made to the IIO subsystem
in the period from 10/03/2018 to 11/17/2018.

Source: [https://www.spinics.net/lists/linux-iio/msg41629.html](
https://www.spinics.net/lists/linux-iio/msg41629.html)

<div class="clearfix"></div>
Due to FLUSP's efforts in contributing to drivers for devices from Analog
Devices, the company donated us four hardware boards. You can see them at the
image below, and an explanation of each one together with useful links (such
as the datasheets) can be found at the [resources page](/resources).

<img src="/img/achievements/donations.png"
     style="display:block; margin: auto;">
